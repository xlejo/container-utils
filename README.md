# Utils on Container

## Contenido:

- wget
- curl
- netcat (nc)
- openssl
- step-cli (step)
- pwgen
- xkcdpass
- bsdtar

## Obteniendo imagen

```bash
$ podman pull registry.gitlab.com/xlejo/container-utils:latest
```

## Guardando en .zshrc (o .bashrc)

```bash
$ vim ~/.zshrc
```

Al principio de la linea poner lo siguiente (en el caso de usar Docker cambiar Podman por Docker):

```
alias ft="podman run --rm -v $(pwd):/home/nobody --security-opt label=disable -w /home/nobody registry.gitlab.com/xlejo/container-utils:latest"
alias fti="podman run --rm -it -v $(pwd):/home/nobody --security-opt label=disable -w /home/nobody registry.gitlab.com/xlejo/container-utils:latest"
```

También se podría hacer directamente con `cat`:

```bash
$ cat >> ~/.{zshrc,bashrc} <<-_EOF
alias ft="podman run --rm -v $(pwd):/home/nobody --security-opt label=disable -w /home/nobody registry.gitlab.com/xlejo/container-utils:latest"
alias fti="podman run --rm -it -v $(pwd):/home/nobody --security-opt label=disable -w /home/nobody registry.gitlab.com/xlejo/container-utils:latest"
_EOF
```

Una vez que se haya guardado, ejecutar: 

```bash
$ source ~/.zshrc
```

## Ejemplos:

Netcat:
```bash
$ ft nc -vz gitlab.com 443
Connection to gitlab.com 443 port [tcp/https] succeeded!
```

OpenSSL:

```bash
$ ft openssl s_client -connect gitlab.com:443
CONNECTED(00000003)
[output truncated]
```

sh: 

```bash
$ fti /bin/sh
/home/nobody #
```

Descomprimiendo .rar:

```bash
$ ft bsdtar -xf example-file.rar
```
